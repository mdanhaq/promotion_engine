﻿using PromotionEngine.SKU_IDs;
using System;

namespace PromotionEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            int totalValue = 0;
            Console.WriteLine("Enter only digit as a number");

            Console.Write("\nEnter the number of A: ");
            int numberOfSKU_ID_A = Convert.ToInt32(Console.ReadLine());

            SKU_ID_A sKU_ID_A = new SKU_ID_A(numberOfSKU_ID_A);
            totalValue += sKU_ID_A.CalculatingValue();

            Console.Write("\nEnter the number of B: ");
            int numberOfSKU_ID_B = Convert.ToInt32(Console.ReadLine());
            SKU_ID_B sKU_ID_B = new SKU_ID_B(numberOfSKU_ID_B);
            totalValue += sKU_ID_B.CalculatingValue();

            Console.Write("\nEnter the number of C: ");
            int numberOfSKU_ID_C = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter the number of D: ");
            int numberOfSKU_ID_D = Convert.ToInt32(Console.ReadLine());

            SKU_ID_CD sKU_ID_CD = new SKU_ID_CD(numberOfSKU_ID_C, numberOfSKU_ID_D);
            totalValue += sKU_ID_CD.CalculatingValue();
            Console.Write("\nTotal value is: " + totalValue);

            Console.ReadLine();

        }
    }
}
