﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine.SKU_IDs
{
    public class SKU_ID_A : ICalculation
    {
        private int divisible { get; set; }
        private int remainder { get; set; }
        private int numberOfUnitA { get; set; }
        public SKU_ID_A(int inputA)
        {
            numberOfUnitA = inputA;
        }
        //SKU_ID_A Calculation
        public int CalculatingValue()
        {
            divisible = numberOfUnitA / 3;
            remainder = numberOfUnitA % 3;

            return divisible * 130 + remainder * 50;
        }
    }
}
