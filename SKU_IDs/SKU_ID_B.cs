﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine.SKU_IDs
{
    public class SKU_ID_B
    {
        private int divisible { get; set; }
        private int remainder { get; set; }
        private int numberOfUnitB { get; set; }
        public SKU_ID_B(int inputB)
        {
            numberOfUnitB = inputB;
        }

        public int CalculatingValue()
        {
            divisible = numberOfUnitB / 2;
            remainder = numberOfUnitB % 2;

            return divisible * 45 + remainder * 30;
        }
    }
}
