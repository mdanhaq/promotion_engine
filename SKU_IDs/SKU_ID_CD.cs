﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine.SKU_IDs
{
    public class SKU_ID_CD 
    {
        private int numberOfUnitC;
        private int numberOfUnitD;
        public SKU_ID_CD(int inputC, int inputD)
        {
            numberOfUnitC = inputC;
            numberOfUnitD = inputD;
        }

        public int CalculatingValue()
        {
            if (numberOfUnitC == numberOfUnitD)
                return numberOfUnitC * 30;
            else
                return (numberOfUnitC * 20 + numberOfUnitD * 15);
        }

    }
}
  
