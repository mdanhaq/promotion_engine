# Promotion_Engine

Implementation of a simple promotion engine for a checkout process

## Assignment Overview

This assignment is written in .net which adopts OOP and clean code practices. It is also accompanied by unit tests 
•   Written in .NET
•	Adopted by OOP and clean code practices
•	Accompanied by unit tests...TDD approach

A simple promotion engine for a checkout process has cart which contains a list of single character SKU ids (A, B, C....) over which the promotion engine will need to run.

The promotion engine calculates the total order value after applying the 2 promotion types
•	buy 'n' items of a SKU for a fixed price (3 A's for 130)
•	buy SKU 1 & SKU 2 for a fixed price ( C + D = 30 )

Test Setup
Unit price for SKU IDs
A      50
B      30
C      20
D      15

Active Promotions
3 of A's for 130
2 of B's for 45
C & D for 30

Scenario A
1 * A     50
1 * B     30
1 * C     20
======
Total     100

Scenario B
5 * A     130 + 2*50
5 * B     45 + 45 + 30
1 * C     20
======
Total     370

Scenario C
3 * A     130
5 * B     45 + 45 + 1 * 30
1 * C     -
1 * D     30
======
Total     280

## Requirements
Microsoft Visual Studio Community 2019
Conosole Application
xUnit Test Project

## Link for Promotion Engine Tests:
https://gitlab.com/mdanhaq/promotionenginetests
