﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine
{
     public static class Validator
    {
        public static bool FieldIsEmpty(string userInput)
        {
            if (String.IsNullOrEmpty(userInput))
                return true;
            return false;
        }

        public static bool InputIsDigit(string userInput)
        {
            if(userInput.All(Char.IsDigit))
                return true;
            return false;
        }
    }
}
